# Plataforma de relacionamento de egressos #

## Modelo de entidade e relacionamento (MER) ##
O MER se encontra no arquivo database.mwb  
Este arquivo deve ser editado apenas através do software MySQL Workbench

## Regulamento ##
O regulamento se encontra no arquivo regulamento.pdf

## Tecnologias ##
### Back-end ###
Para back-end será desenvolvido uma API utilizando a linguagem PHP junto com o framework Lumen
### Front-end ###
Para front-end será utilizado HTML5, CSS3 e Java Script com a biblioteca VueJS

## Interfaces ##
* Login (estudante, empresa, administrador)

### Interface de administrador ###
* Controlar estudante/empresa
* Controlar survey (Questionário)
* Dashboard

### Interfaces de estudante ###
* Atualizar dados
* Controlar comunidade virtual
* Controlar forum
* Comunidades virtuais
  * Comunidade virtual
    * Foruns
      * Forum
* Visualizar vagas

### Interfaces de empresa ###
* Atualizar dados
* Controlar comunidade virtual
* Controlar forum
* Comunidades virtuais
  * Comunidade virtual
    * Foruns
      * Forum
* Controlar vagas de emprego
